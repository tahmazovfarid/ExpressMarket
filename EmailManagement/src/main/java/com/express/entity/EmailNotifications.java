package com.express.entity;

import com.express.enums.ResponseStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class EmailNotifications implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @Column(unique = true)
    String email;
    String token;
    String password;
    @Builder.Default
    LocalDateTime createDate = LocalDateTime.now();
    LocalDateTime confirmedAt;
    @Builder.Default
    LocalDateTime expiredAt = LocalDateTime.now().plusHours(24);
    @Builder.Default @Enumerated(value = EnumType.STRING)
    ResponseStatus responseStatus = ResponseStatus.ACCEPTED;
    Integer tempUserId;
}
