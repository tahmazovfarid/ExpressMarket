package com.express.repository;

import com.express.entity.EmailNotifications;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public interface EmailNotificationsRepo extends JpaRepository<EmailNotifications, Integer> {
    Optional<EmailNotifications> findByToken(String token);

    @Transactional
    @Modifying
    @Query("UPDATE EmailNotifications e SET e.confirmedAt = :confirmedAt WHERE e.token = :token")
    int updateConfirmedAt(@Param("token") String token, @Param("confirmedAt") LocalDateTime confirmedAt);

    Optional<EmailNotifications> findByEmail(String email);
    Optional<EmailNotifications> findByTempUserId(Integer tempUserId);
}

