package com.express.service;

import com.express.dto.ResponseDTO;
import com.express.dto.TempUserDTO;
import com.express.entity.EmailNotifications;
import com.express.entity.util.PasswordGenerator;
import com.express.repository.EmailNotificationsRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.Email;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@Service
@RequiredArgsConstructor
public class EmailNotificationsService {
    private final EmailNotificationsRepo emailNotificationsRepo;
    private final EmailService emailService;

    public void save(EmailNotifications emailNotifications){
        emailNotificationsRepo.save(emailNotifications);
    }

    public List<EmailNotifications> emailNotificationsList(){
        return emailNotificationsRepo.findAll();
    }

    public int setConfirmedAt(String token) {
        return emailNotificationsRepo.updateConfirmedAt(token, LocalDateTime.now());
    }

    public EmailNotifications findByToken(String token){
        return emailNotificationsRepo.findByToken(token)
                .orElseThrow(() -> new ResponseStatusException(NOT_FOUND));
    }

    public EmailNotifications findById(Integer id){
        return emailNotificationsRepo.findById(id)
                .orElseThrow(()-> new ResponseStatusException(NOT_FOUND));
    }

    public EmailNotifications findByEmail(String email){
        return emailNotificationsRepo.findByEmail(email)
                .orElseThrow(()-> new ResponseStatusException(NOT_FOUND));
    }

    public EmailNotifications findByTempId(Integer tempUserId){
        return emailNotificationsRepo.findByTempUserId(tempUserId)
                .orElseThrow(()-> new ResponseStatusException(NOT_FOUND));
    }




}
