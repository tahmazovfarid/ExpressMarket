package com.express.service;

import com.express.entity.util.PasswordGenerator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.UUID;

@Service @Slf4j @RequiredArgsConstructor
public class UtilService {
    private final RestTemplate restTemplate;

    public <T> T sendGetApi(String url, Class<T> t){
        return restTemplate.getForObject(url, t);
    }

    public <T> T sendPostApi(String url, T object, Class<T> tClass){
        return restTemplate.postForObject(url, object, tClass);
    }

    public String passwordGeneration(){
        PasswordGenerator passwordGenerator = new PasswordGenerator.PasswordGeneratorBuilder()
                .useDigits(true)
                .useLower(true)
                .useUpper(true)
                .usePunctuation(true)
                .build();
        return passwordGenerator.generate(10);
    }

    public String tokenGeneration(){
        return UUID.randomUUID().toString();
    }
}
