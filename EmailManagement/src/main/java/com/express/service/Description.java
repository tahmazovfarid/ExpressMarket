package com.express.service;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Description {
    static String DATA_SAVED = "data is saved!";
    static String CHECK_EMAIL = "Check email box!";
    static String TOKEN_IS_INVALID = "Token is invalid!";
    static String USER_IS_NOT_CONFIRMED = "User is not confirmed!";
    static String INCORRECT_PASSWORD = "Password is incorrect!";
    static String PASSWORD_DONT_MATCHES = "New password and confirm password dont matches!";
    static String TOKEN_IS_VALID = "Token is valid!";
    static String USER_IS_CONFIRMED = "User is confirmed!";
    static String EMAIL_ALREADY_CONFIRMED = "Email already confirmed!";
    static String TOKEN_IS_EXPIRED = "Token is expired!";
    static String FAILED_TO_SEND_MAIL = "Failed to send email!";
}
