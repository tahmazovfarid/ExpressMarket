package com.express.service;

import com.express.dto.PasswordDTO;
import com.express.dto.ResponseDTO;
import com.express.dto.TempUserDTO;
import com.express.dto.UserDTO;
import com.express.entity.EmailNotifications;
import com.express.enums.ResponseStatus;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import static com.express.service.Description.*;

import java.time.LocalDateTime;
import java.util.Objects;

@Service @Slf4j
@RequiredArgsConstructor
@PropertySource("../../../../resources/url.properties")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SignService {

    final EmailNotificationsService emailNotificationsService;
    final EmailService emailService;
    final UtilService utilService;
    final Environment environment;

    @Value("${verify.email}")
    final String sendMailApi;

    //Save email
    public ResponseEntity<ResponseDTO<String>> saveEmailDb(TempUserDTO tempUserDTO){
        String token = utilService.tokenGeneration();

        EmailNotifications emailNotifications = EmailNotifications
                .builder()
                .token(token)
                .password(utilService.passwordGeneration())
                .tempUserId(tempUserDTO.getId())
                .email(tempUserDTO.getEmail()).build();
        emailNotificationsService.save(emailNotifications);

        ResponseDTO<String> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, DATA_SAVED, token);

        return ResponseEntity.ok().body(responseDTO);
    }

    //Send mail
    public ResponseEntity<ResponseDTO<String>> sendMail(Integer id){
        EmailNotifications emailNotifications = emailNotificationsService.findById(id);
        String email = emailNotifications.getEmail();
        String token = emailNotifications.getToken();

        String link = sendMailApi+token;
        emailService.sendVerifyAccountMsg(email, buildEmail(link));
        log.info("link: {}", link);
        log.info("email send..");

        ResponseDTO<String> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, CHECK_EMAIL,
                token + " " + emailNotifications.getPassword());

        return ResponseEntity.ok().body(responseDTO);
    }

    //confirm user
    public ResponseEntity<ResponseDTO> confirmUser(String token, PasswordDTO passwordDTO){
        EmailNotifications emailNotifications = emailNotificationsService.findByToken(token);
        boolean isValidToken = Objects.requireNonNull(confirmToken(token).getBody()).getObject();

        ResponseStatus status = ResponseStatus.BAD_REQUEST;
        String desc = TOKEN_IS_INVALID + "\n" + USER_IS_NOT_CONFIRMED;

        if (isValidToken) {
            desc = INCORRECT_PASSWORD;
            if (emailNotifications.getPassword().equals(passwordDTO.getOldPassword())) {
                desc = PASSWORD_DONT_MATCHES;
                log.info("old password is true!");
                if (passwordDTO.getNewPassword().equals(passwordDTO.getConfirmPassword())) {
                    String url = environment.getProperty("send.user.request");
                    UserDTO userDTO = UserDTO.builder().tempUserId(emailNotifications.getTempUserId()).password(passwordDTO.getConfirmPassword()).build();
                    emailNotificationsService.setConfirmedAt(token);
                    utilService.sendPostApi(url, userDTO, UserDTO.class);
                    status = ResponseStatus.SUCCESS;
                    desc = TOKEN_IS_VALID + "\n" + emailNotifications.getEmail() + " " + USER_IS_CONFIRMED;
                    log.info("user dto is send..!");
                }
            }
        }
        ResponseDTO responseDTO = ResponseDTO.of(status, desc);
        return ResponseEntity.ok().body(responseDTO);
    }

    //confirm token
    public ResponseEntity<ResponseDTO<Boolean>> confirmToken(String token){
        EmailNotifications emailNotifications = emailNotificationsService.findByToken(token);
        ResponseStatus status = ResponseStatus.SUCCESS;
        String desc = TOKEN_IS_VALID;
        boolean isValidToken = true;

        if (emailNotifications.getConfirmedAt() != null) {
            status = ResponseStatus.FOUND;
            desc = EMAIL_ALREADY_CONFIRMED;
            isValidToken = false;
        }
        LocalDateTime expiredAt = emailNotifications.getExpiredAt();
        if (expiredAt.isBefore(LocalDateTime.now())) {
            status = ResponseStatus.EXPIRED;
            desc = TOKEN_IS_EXPIRED;
            isValidToken = false;
        }

        ResponseDTO<Boolean> responseDto = ResponseDTO.of(status, desc, isValidToken);
        return ResponseEntity.ok().body(responseDto);
    }

    private String buildEmail(String link) {
        return "emailTemplate";
    }
}
