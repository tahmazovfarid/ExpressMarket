package com.express.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor @NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PasswordDTO {
    String oldPassword;
    String newPassword;
    String confirmPassword;
}
