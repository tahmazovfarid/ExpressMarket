package com.express.dto;

import com.express.enums.ResponseStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor @NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ResponseDTO<T> {
    ResponseStatus status;
    String desc;
    T object;

    public static ResponseDTO of(ResponseStatus status, String desc){
        return ResponseDTO.builder().status(status).desc(desc).build();
    }

    public static <T> ResponseDTO of(ResponseStatus status, String desc, T object){
        return ResponseDTO.builder().status(status).desc(desc).object(object).build();
    }
}
