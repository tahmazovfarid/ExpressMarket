package com.express.controller;

import com.express.dto.PasswordDTO;
import com.express.dto.TempUserDTO;
import com.express.dto.ResponseDTO;
import com.express.entity.EmailNotifications;
import com.express.enums.ResponseStatus;
import com.express.service.EmailNotificationsService;
import com.express.service.SignService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${request.mapping}")
@RequiredArgsConstructor
public class EmailController {
    private final EmailNotificationsService emailNotificationsService;
    private final SignService signService;

    @PostMapping
    public ResponseEntity<ResponseDTO<String>> getDTO(@RequestBody TempUserDTO tempUserDTO){
        return signService.saveEmailDb(tempUserDTO);
    }

    @PostMapping("${send.mail}/{id}")
    public ResponseEntity<ResponseDTO<String>> sendMail(@PathVariable Integer id){
        return signService.sendMail(id);
    }

    @PostMapping("${token.confirm}")
    public ResponseEntity<ResponseDTO<Boolean>> confirmToken(@RequestParam("token") String token){
        return signService.confirmToken(token);
    }

    @PostMapping("${user.confirm}")
    public ResponseEntity<ResponseDTO> confirmUser(@RequestParam("token") String token, @RequestBody PasswordDTO passwordDTO){
        return signService.confirmUser(token, passwordDTO);
    }

    @GetMapping("${get.all}")
    public ResponseEntity<ResponseDTO<List<EmailNotifications>>> getAll(){
        ResponseDTO<List<EmailNotifications>> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, "Success!", emailNotificationsService.emailNotificationsList());
        return ResponseEntity.ok().body(responseDTO);
    }
}
