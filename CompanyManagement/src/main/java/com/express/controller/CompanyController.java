package com.express.controller;

import com.express.dto.CompanyDTO;
import com.express.dto.CompanyUserDTO;
import com.express.dto.ResponseDTO;
import com.express.service.ApiService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController @RequestMapping("${request.mapping}")
@RequiredArgsConstructor @Slf4j
public class CompanyController {
    private final ApiService apiService;

    @PostMapping
    public ResponseEntity<ResponseDTO<CompanyDTO>> getDTO(@RequestBody CompanyDTO companyDTO){
        return apiService.saveCompany(companyDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO<CompanyUserDTO>> getCompanyUsers(@PathVariable int id){
        return apiService.getCompanyUsers(id);
    }
}
