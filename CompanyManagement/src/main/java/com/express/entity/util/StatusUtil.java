package com.express.entity.util;

public enum StatusUtil {
    WAITING,
    EXPIRED,
    IGNORE,
    ACTIVE,
    NOT_ACTIVE,
    ACCEPTED
}
