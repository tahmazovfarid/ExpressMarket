package com.express.entity;

import com.express.entity.util.StatusUtil;
import com.express.enums.ResponseStatus;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CompanyUser {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    Integer companyId;
    @Column(unique = true)
    String userEmail;
    @Builder.Default
    LocalDateTime createDate = LocalDateTime.now();
    @Builder.Default @Enumerated(value = EnumType.STRING)
    ResponseStatus status = ResponseStatus.PENDING;
}
