package com.express.repo;

import com.express.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CompanyRepo extends JpaRepository<Company, Integer> {
    Optional<Company> findByName(String companyName);
}
