package com.express.repo;

import com.express.entity.CompanyUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyUserRepo extends JpaRepository<CompanyUser, Integer> {
    List<CompanyUser> findCompanyUserByCompanyId(Integer companyId);
}
