package com.express.service;

import com.express.dto.CompanyDTO;
import com.express.dto.CompanyUserDTO;
import com.express.dto.ResponseDTO;
import com.express.entity.Company;
import com.express.entity.CompanyUser;
import com.express.enums.ResponseStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import static com.express.service.Description.*;

@Service @RequiredArgsConstructor @Slf4j
public class ApiService {
    private final CompanyService companyService;
    private final CompanyUserService companyUserService;
    private final ModelMapper mapper;

    public ResponseEntity<ResponseDTO<CompanyDTO>> saveCompany(CompanyDTO companyDTO){
        Company company = companyService.findByCompanyName(companyDTO.getName());

        if (company == null){
            company = mapper.map(companyDTO, Company.class);
            companyService.save(company);
            log.info("company is save!");
        }
        CompanyUser companyUser = CompanyUser.builder()
                .companyId(company.getId())
                .userEmail(companyDTO.getUserEmail()).build();
        companyUserService.save(companyUser);

        ResponseDTO<CompanyDTO> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, DATA_SAVED, companyDTO);

        return ResponseEntity.ok().body(responseDTO);
    }

    public ResponseEntity<ResponseDTO<CompanyUserDTO>> getCompanyUsers(int id){
        List<CompanyUser> companyUsers = companyUserService.findByCompanyId(id);
        CompanyUserDTO companyUserDTO = new CompanyUserDTO();

        ResponseStatus status = ResponseStatus.BAD_REQUEST;
        String desc = COMPANY_IS_NULL;

        if (!companyUsers.isEmpty()){
            status = ResponseStatus.SUCCESS;
            desc = SUCCESS;
            List<String> userEmails = new ArrayList<>();
            companyUserDTO.setCompanyName(companyService.findById(companyUsers.get(0).getCompanyId()).getName());
            for (CompanyUser companyUser: companyUsers) {
                userEmails.add(companyUser.getUserEmail());
            }
            companyUserDTO.setUserEmails(userEmails);
        }

        ResponseDTO<CompanyUserDTO> responseDTO = ResponseDTO.of(status, desc, companyUserDTO);
        return ResponseEntity.ok().body(responseDTO);
    }
}
