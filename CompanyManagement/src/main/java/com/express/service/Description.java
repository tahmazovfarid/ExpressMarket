package com.express.service;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class Description {
    static String DATA_SAVED = "data is saved!";
    static String COMPANY_IS_NULL = "Company is null!";
    static String SUCCESS = "Success!";
}
