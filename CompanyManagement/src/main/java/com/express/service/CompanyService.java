package com.express.service;

import com.express.entity.Company;
import com.express.repo.CompanyRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service @RequiredArgsConstructor @Slf4j
public class CompanyService {
    private final CompanyRepo companyRepo;

    public void save(Company company){
        companyRepo.save(company);
    }

    public Company findById(Integer id){
        return companyRepo.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public Company findByCompanyName(String companyName){
        Optional<Company> company = companyRepo.findByName(companyName);
        if (company.isEmpty()){
            return null;
        }
        return company.get();
    }

    public List<Company> findAll(){
        return companyRepo.findAll();
    }
}
