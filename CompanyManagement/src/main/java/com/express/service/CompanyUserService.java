package com.express.service;

import com.express.entity.CompanyUser;
import com.express.repo.CompanyUserRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service @RequiredArgsConstructor @Slf4j
public class CompanyUserService {
    private final CompanyUserRepo companyUserRepo;

    public void save(CompanyUser companyUser){
        companyUserRepo.save(companyUser);
    }

    public CompanyUser findById(Integer id){
        return companyUserRepo.findById(id).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public List<CompanyUser> findAll(){
        return companyUserRepo.findAll();
    }

    public List<CompanyUser> findByCompanyId(Integer companyId){
        return companyUserRepo.findCompanyUserByCompanyId(companyId);
    }
}
