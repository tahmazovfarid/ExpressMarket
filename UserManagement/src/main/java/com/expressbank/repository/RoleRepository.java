package com.expressbank.repository;

import com.expressbank.entity.Role;
import com.expressbank.entity.util.RoleUtil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByRoleUtil(RoleUtil roleUtil);
}
