package com.expressbank.repository;

import com.expressbank.entity.Permission;
import com.expressbank.entity.User;
import com.expressbank.entity.UserPermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserPermissionRepository extends JpaRepository<UserPermission, Integer> {
    Optional<List<UserPermission>> findByPermission(Permission permission);
    Optional<List<UserPermission>> findByUser(User user);
}
