package com.expressbank.repository;

import com.expressbank.entity.TempUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TempUserRepository extends JpaRepository<TempUser, Integer> {
    TempUser findTempUsersByEmail(String email);
}
