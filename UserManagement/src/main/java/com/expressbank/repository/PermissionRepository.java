package com.expressbank.repository;

import com.expressbank.entity.Permission;
import com.expressbank.entity.util.PermissionUtil;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, Integer> {
    Optional<Permission> findPermissionByPermissionUtil(PermissionUtil permissionUtil);
}
