package com.expressbank.entity.util;

public enum PermissionUtil {
    CREATE,
    READ,
    UPDATE,
    DELETE
}
