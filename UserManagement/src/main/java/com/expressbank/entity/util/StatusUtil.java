package com.expressbank.entity.util;

public enum StatusUtil {
    WAITING,
    EXPIRED,
    IGNORE,
    ACTIVE,
    NOT_ACTIVE,
    ACCEPTED
}
