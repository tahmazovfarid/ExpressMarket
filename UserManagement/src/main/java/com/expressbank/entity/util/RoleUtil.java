package com.expressbank.entity.util;

public enum RoleUtil {
    SUPER_ADMIN, //employee
    ADMIN, //bussinesmen
    MODERATOR, //seller
    USER //customer
}
