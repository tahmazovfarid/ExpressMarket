package com.expressbank.entity;

import com.expressbank.entity.util.RoleUtil;
import com.expressbank.entity.util.StatusUtil;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor @NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Role implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    RoleUtil roleUtil;
    @Builder.Default
    LocalDateTime createDate = LocalDateTime.now();
    @Builder.Default @Enumerated(value = EnumType.STRING)
    StatusUtil statusUtil = StatusUtil.ACTIVE;
}
