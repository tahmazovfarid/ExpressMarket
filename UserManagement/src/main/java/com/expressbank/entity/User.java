package com.expressbank.entity;

import com.expressbank.entity.util.StatusUtil;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor @NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String surname;
    LocalDate birthdate;
    String phone;
    String email;
    String password;
    Integer superId;
    Integer companyId;
    @ManyToOne
    Role Role;
    @OneToOne
    TempUser tempUser;
    @Builder.Default
    LocalDateTime createDate = LocalDateTime.now();
    @Builder.Default @Enumerated(value = EnumType.STRING)
    StatusUtil statusUtil = StatusUtil.ACTIVE;
}
