package com.expressbank.entity;

import com.expressbank.entity.util.StatusUtil;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Data
@Builder
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserPermission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @OneToOne
    User user;
    @OneToOne
    Permission permission;
    @Builder.Default
    LocalDateTime createDate = LocalDateTime.now();
    @Builder.Default @Enumerated(value = EnumType.STRING)
    StatusUtil statusUtil = StatusUtil.ACTIVE;

    public UserPermission() {}
}
