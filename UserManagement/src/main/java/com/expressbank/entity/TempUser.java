package com.expressbank.entity;

import com.expressbank.entity.util.StatusUtil;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data @Builder
@AllArgsConstructor @NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TempUser implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String name;
    String surname;
    LocalDate birthdate;
    String phone;
    String email;
    String company;
    @CreationTimestamp
    LocalDateTime createDate;
    @Builder.Default @Enumerated(value = EnumType.STRING)
    StatusUtil statusUtil = StatusUtil.WAITING;
}
