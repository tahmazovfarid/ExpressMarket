package com.expressbank;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Main {
    private Integer x;
    private Integer y;
    private Integer z;

    private Main(){}

    private static Main main = null;

    public static Main newInstance() {
       if (main==null){
           main = new Main();
           return main;
       }

       return main;
    }
}
