package com.expressbank.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@RequiredArgsConstructor
@Getter @ToString
public enum ResponseStatus {
    CONTINUE(100),
    PROCESSING(102),
    PENDING(103), //
    SUCCESS(200),
    CREATED(201),
    ACCEPTED(202),
    NO_CONTENT(204),
    FOUND(302),
    ACTIVE(380), //
    BAD_REQUEST(400),
    NOT_ACTIVE(402), //
    FORBIDDEN(403),
    NOT_FOUND(404),
    EXPIRED(405), //
    IGNORE(406), //
    INTERNAL_SERVER_ERROR(500);

    final int code;
}
