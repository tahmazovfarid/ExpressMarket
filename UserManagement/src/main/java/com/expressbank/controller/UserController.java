package com.expressbank.controller;

import com.expressbank.dto.*;
import com.expressbank.entity.User;
import com.expressbank.service.RegistrationService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.http.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserController {
    private final RegistrationService registrationService;

    //register
    @SneakyThrows
    @PostMapping("/register")
    public ResponseEntity<ResponseDTO<TempUserDTO>> register(@RequestBody @Valid TempUserDTO tempUserDTO, BindingResult result){
      return registrationService.register(tempUserDTO, result);
    }

    @PostMapping("/save")
    public ResponseEntity<ResponseDTO<User>> saveUser(@RequestBody UserReqDTO userReqDTO){
       return registrationService.saveUser(userReqDTO);
    }

    //Get by id
    @GetMapping("/list/temp/{id}")
    public ResponseEntity<ResponseDTO<TempUserDTO>> getTempData(@PathVariable Integer id){
       return registrationService.getTempData(id);
    }

    //Get all
    @GetMapping("/list/temp")
    public ResponseEntity<ResponseDTO<List<TempUserDTO>>> getAllTempData(){
        return registrationService.getAllTempData();
    }

    @GetMapping("/list")
    public ResponseEntity<ResponseDTO<List<UserRepDTO>>> getAllUsers(){
        return registrationService.getAllUsers();
    }

    @GetMapping("/list/{id}")
    public ResponseEntity<ResponseDTO<UserRepDTO>> getUser(@PathVariable int id){
        return registrationService.getUser(id);
    }

    @PostMapping("/role/add-to-user")
    public ResponseEntity<?> addRoleToUser(@RequestBody RoleToUserForm form){
        return registrationService.addRoleToUser(form);
    }

    @GetMapping("/email/{email}")
    public ResponseEntity<ResponseDTO<ProductUserId>> receiveEmailAndSendId(@PathVariable String email){
        return null;
    }
}
