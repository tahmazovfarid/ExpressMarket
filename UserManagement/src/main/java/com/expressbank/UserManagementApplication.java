package com.expressbank;

import com.expressbank.entity.Role;
import com.expressbank.entity.util.RoleUtil;
import com.expressbank.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication @RequiredArgsConstructor
public class UserManagementApplication {
    private final RoleService roleService;

    public static void main(String[] args) {
        SpringApplication.run(UserManagementApplication.class, args);
    }

//    @Bean
//    public BCryptPasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }

    @Bean
    public CommandLineRunner CommandLineRunnerBean() {
        return (args) -> {
            List<Role> roleList = new ArrayList<>();
            roleList.add(Role.builder().roleUtil(RoleUtil.SUPER_ADMIN).build());
            roleList.add(Role.builder().roleUtil(RoleUtil.ADMIN).build());
            roleList.add(Role.builder().roleUtil(RoleUtil.MODERATOR).build());
            roleList.add(Role.builder().roleUtil(RoleUtil.USER).build());

            roleService.saveAll(roleList);
        };
    }
    @Bean
    public static ModelMapper modelMapper() {
        return new ModelMapper();
    }
    @Bean
    public static RestTemplate getRestTemplate() {
        return new RestTemplate();
    }
}
