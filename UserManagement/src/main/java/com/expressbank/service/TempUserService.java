package com.expressbank.service;

import com.expressbank.dto.EmailDTO;
import com.expressbank.entity.TempUser;
import com.expressbank.repository.TempUserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.Email;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor @Slf4j
public class TempUserService {
    private final TempUserRepository tempUserRepository;
    private final RestTemplate restTemplate;

    public void save(TempUser tempUser){
        tempUserRepository.save(tempUser);
    }

    public TempUser findById(Integer id){
        return tempUserRepository.findById(id)
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public TempUser findByEmail(String email){
        return tempUserRepository.findTempUsersByEmail(email);
    }

    public List<TempUser> findAll(){
        return tempUserRepository.findAll();
    }

}
