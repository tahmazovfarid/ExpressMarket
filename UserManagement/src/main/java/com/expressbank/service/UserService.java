package com.expressbank.service;

import com.expressbank.entity.Permission;
import com.expressbank.entity.User;
import com.expressbank.entity.UserPermission;
import com.expressbank.entity.util.PermissionUtil;
import com.expressbank.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service @Slf4j
@RequiredArgsConstructor
public class UserService
        {
    private final UserRepository userRepository;
    private final PermissionService permissionService;
//    private final PasswordEncoder passwordEncoder;
    private final UserPermissionService userPermissionService;
    private final static String USER_NOT_FOUND_MSG = "Username: %s not found in the database!";
    private final static String USER_ID_NOT_FOUND_MSG = "User ID: %s not found in the database!";

    public void save(User user){
//        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        log.info("user is saved!");
    }

    public User findByEmail(String email){
        Optional<User> user =  userRepository.findByEmail(email);
        if (user.isEmpty()){
            log.error("Username: {} not found in the database!", email);
//            throw new UsernameNotFoundException(String.format(USER_NOT_FOUND_MSG, email));
        } else{
            log.info("Username: {} found in the database!", email);
        }
        return user.get();
    }

//    @Override
//    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
//        User user = findByEmail(email);
//        List<UserPermission> userPermissions = userPermissionService.findByUser(user);
//
//        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
//
//        authorities.add(new SimpleGrantedAuthority(user.getRole().getRoleUtil().toString()));
//
//        for (UserPermission userPermission: userPermissions){
//            authorities.add(new SimpleGrantedAuthority(userPermission.getPermission().getPermissionUtil().toString()));
//        }
//
//        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), authorities);
//    }

//    @PreAuthorize("@opaClient.allow('read', T(java.util.Map).of('type', 'user'))")
    public List<User> findAll(){
        return userRepository.findAll();
    }

//    @PreAuthorize("@opaClient.allow('read', T(java.util.Map).of('type', 'user', 'id', #id))")
    public User findById(Integer id){
        Optional<User> user =  userRepository.findById(id);
        if (user.isEmpty()){
            log.error("User ID: {} not found in the database!", id);
//            throw new UsernameNotFoundException(String.format(USER_ID_NOT_FOUND_MSG, id));
        } else{
            log.info("User ID: {} found in the database!", id);
        }
        return user.get();
    }

    public void addPermissionToUser(String email, String permissionUtil) {
        log.info("Adding permission {} to user {} !", permissionUtil.toString(), email);
        User user = findByEmail(email);
        Permission permission = permissionService.findByPermissionUtil(PermissionUtil.valueOf(permissionUtil));

        UserPermission userPermission = UserPermission.builder()
                .user(user)
                .permission(permission).build();
        userPermissionService.save(userPermission);
    }
}
