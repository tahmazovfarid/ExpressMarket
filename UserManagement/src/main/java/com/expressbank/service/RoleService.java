package com.expressbank.service;

import com.expressbank.entity.User;
import com.expressbank.entity.Role;
import com.expressbank.entity.util.RoleUtil;
import com.expressbank.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository roleRepository;

    public void save(Role role) {
        roleRepository.save(role);
    }

    public void saveAll(List<Role> roleList){
        roleRepository.saveAll(roleList);
    }

    public void delete(Integer id){
        roleRepository.deleteById(id);
    }

    public Optional<Role> findById(Integer id){
        return roleRepository.findById(id);
    }

    public Role findByRoleUtil(RoleUtil roleUtil){
        return roleRepository.findByRoleUtil(roleUtil).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }
}
