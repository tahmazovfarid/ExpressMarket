package com.expressbank.service;

import com.expressbank.entity.Permission;
import com.expressbank.entity.util.PermissionUtil;
import com.expressbank.repository.PermissionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PermissionService {
    private final PermissionRepository permissionRepository;

    public void save(Permission permission){
        permissionRepository.save(permission);
    }

    public void saveAll(List<Permission> permissionList){
        permissionRepository.saveAll(permissionList);
    }

    public Permission findByPermissionUtil(PermissionUtil permissionUtil){
        return permissionRepository.findPermissionByPermissionUtil(permissionUtil)
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

}
