package com.expressbank.service;

import com.expressbank.dto.*;
import com.expressbank.entity.TempUser;
import com.expressbank.entity.User;
import com.expressbank.entity.util.RoleUtil;
import com.expressbank.enums.ResponseStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.web.client.RestTemplate;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Service @RequiredArgsConstructor
@Slf4j
public class RegistrationService {
    private final TempUserService tempUserService;
    private final RoleService roleService;
    private final RestTemplate restTemplate;
    private final UserService userService;
    private final ModelMapper mapper;

    public ResponseEntity<ResponseDTO<UserRepDTO>> getUser(int id){
        User user = userService.findById(id);
        UserRepDTO userRepDTO = mapper.map(user, UserRepDTO.class);
        userRepDTO.setCompanyName(user.getTempUser().getCompany());

        ResponseDTO<UserRepDTO> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, "user list", userRepDTO);

        return ResponseEntity.ok().body(responseDTO);
    }

    public ResponseEntity<?> addRoleToUser(RoleToUserForm form){
        userService.addPermissionToUser(form.getEmail(), form.getPermission());
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<ResponseDTO<List<UserRepDTO>>> getAllUsers(){
        List<User> users = userService.findAll();
        List<UserRepDTO> userRepDTOS = new ArrayList<>();
        for (User user: users) {
            UserRepDTO userRepDTO = mapper.map(user, UserRepDTO.class);
            userRepDTO.setCompanyName(user.getTempUser().getCompany());
            userRepDTOS.add(userRepDTO);
        }
        ResponseDTO<List<UserRepDTO>> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, "user list", userRepDTOS);

        return ResponseEntity.ok().body(responseDTO);
    }

    public ResponseEntity<ResponseDTO<TempUserDTO>> register(@Valid TempUserDTO tempUserDTO, BindingResult result){
        ResponseStatus status = ResponseStatus.SUCCESS;
        String desc = "Register success!";

        if (result.hasErrors()){
            status = ResponseStatus.BAD_REQUEST;
            desc = "Object is null!";
        }

        TempUser tempUser = mapper.map(tempUserDTO, TempUser.class);
        tempUserService.save(tempUser);
        sendEmailDTO(tempUser.getEmail());
        sendCompanyDTO(tempUser.getEmail());
        ResponseDTO<TempUserDTO> responseDTO = ResponseDTO.of(status, desc, tempUserDTO);

        return ResponseEntity.ok().body(responseDTO);
    }

    public ResponseEntity<ResponseDTO<User>> saveUser( UserReqDTO userReqDTO){
        TempUser tempUser = tempUserService.findById(userReqDTO.getTempUserId());
        TempUserDTO tempUserDTO = mapper.map(tempUser, TempUserDTO.class);
        User user = mapper.map(tempUserDTO, User.class);
        user.setPassword(userReqDTO.getPassword());
        user.setTempUser(tempUser);
        user.setRole(roleService.findByRoleUtil(RoleUtil.ADMIN));
        userService.save(user);
        ResponseDTO<User> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, "saved user!", user);

        return ResponseEntity.ok().body(responseDTO);
    }

    public ResponseEntity<ResponseDTO<TempUserDTO>> getTempData(Integer id){
        TempUser tempUser = tempUserService.findById(id);
        TempUserDTO tempUserDTO = mapper.map(tempUser, TempUserDTO.class);

        ResponseDTO<TempUserDTO> responseDTO = ResponseDTO.of(ResponseStatus.SUCCESS, "Success!", tempUserDTO);

        return ResponseEntity.ok().body(responseDTO);
    }

    public ResponseEntity<ResponseDTO<List<TempUserDTO>>> getAllTempData(){
        List<TempUser> tempUserList = tempUserService.findAll();
        List<TempUserDTO> tempUserDTOS = new ArrayList<>();
        ResponseStatus status = ResponseStatus.SUCCESS;
        String desc = "Success!";

        for (TempUser tempUser: tempUserList) {
            tempUserDTOS.add(mapper.map(tempUser, TempUserDTO.class));
        }

        if (tempUserList.isEmpty()){
            status = ResponseStatus.NO_CONTENT;
            desc = "Temp user is empty!";
        }

        ResponseDTO<List<TempUserDTO>> responseDto = ResponseDTO.of(status, desc, tempUserDTOS);

        return ResponseEntity.ok().body(responseDto);
    }

    public void sendEmailDTO(String email){
        String url = "http://localhost:8181/api/email/";

        TempUser tempUser = tempUserService.findByEmail(email);
        EmailDTO emailDTO = EmailDTO.builder()
                .id(tempUser.getId())
                .email(tempUser.getEmail()).build();

        sendPostApi(url, emailDTO, EmailDTO.class);
    }

    public ResponseEntity<ResponseDTO<ProductUserId>> receiveEmailAndSendId(String email){
        String url = "";
        User user = userService.findByEmail(email);
        ProductUserId userId = ProductUserId.builder().id(user.getId()).build();

        sendGetApi(url, ProductUserId.class);
        return null;
    }

    public void sendCompanyDTO(String email){
        String url = "http://localhost:8282/api/company/";
        TempUser tempUser = tempUserService.findByEmail(email);
        CompanyDTO companyDTO = CompanyDTO.builder()
                .name(tempUser.getCompany())
                .userEmail(email).build();

        sendPostApi(url, companyDTO, CompanyDTO.class);
    }

    public <T> T sendGetApi(String url, Class<T> t){
        return restTemplate.getForObject(url, t);
    }

    public <T> T sendPostApi(String url, T object, Class<T> tClass){
        return restTemplate.postForObject(url, object, tClass);
    }
}
