package com.expressbank.service;

import com.expressbank.entity.Permission;
import com.expressbank.entity.User;
import com.expressbank.entity.UserPermission;
import com.expressbank.repository.UserPermissionRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Service @Slf4j
@RequiredArgsConstructor
public class UserPermissionService {
    private final UserPermissionRepository userPermissionRepository;

    public void save(UserPermission userPermission){
        userPermissionRepository.save(userPermission);
    }

    public void saveAll(List<UserPermission> userPermissions){
        userPermissionRepository.saveAll(userPermissions);
    }

    public List<UserPermission> findByPermission(Permission permission){
        return userPermissionRepository.findByPermission(permission)
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    public List<UserPermission> findByUser(User user){
        return userPermissionRepository.findByUser(user)
                .orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

}
