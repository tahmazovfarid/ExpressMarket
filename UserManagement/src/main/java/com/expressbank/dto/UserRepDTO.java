package com.expressbank.dto;

import com.expressbank.entity.Role;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserRepDTO {
    String name;
    String surname;
    LocalDate birthdate;
    String phone;
    String email;
    String password;
    Integer superId;
    String companyName;
    Role Role;
}
