package com.expressbank.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TempUserDTO {

    @NotBlank @NotNull(message = "name is null!")
    String name;
    @NotBlank @NotNull(message = "surname is null!")
    String surname;
    @NotNull(message = "birthdate is null!")
    LocalDate birthdate;
    @NotBlank @NotNull(message = "phone is null!")
    String phone;
    @NotBlank @NotNull(message = "email is null!")
    String email;
    @NotBlank @NotNull(message = "company is null!")
    String company;
}
